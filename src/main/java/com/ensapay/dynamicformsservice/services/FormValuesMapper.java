package com.ensapay.dynamicformsservice.services;

import com.ensapay.dynamicformsservice.entities.FormSubmission;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface FormValuesMapper {
    public Map<String, Object> mapFormFieldsToValues(int creanceId, HashMap<String, ?> values) throws Exception;
}
