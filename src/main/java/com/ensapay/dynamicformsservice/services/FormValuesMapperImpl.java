package com.ensapay.dynamicformsservice.services;

import com.ensapay.dynamicformsservice.entities.FormField;
import com.ensapay.dynamicformsservice.exceptions.NullFormFieldValueException;
import com.ensapay.dynamicformsservice.respostiories.CreanceRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class FormValuesMapperImpl implements FormValuesMapper {

    private CreanceRepository creanceRepository;

    @Override
    public Map<String, Object> mapFormFieldsToValues(int creanceId, HashMap<String, ?> values) {
        List<FormField> fields = creanceRepository
            .findById(creanceId)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND))
            .getFields();

        Map<String, Object> submittedValues = new HashMap<>();

        fields.forEach(
            formField -> {
                if (!values.containsKey(formField.getName())) {
                    if (formField.isRequired())
                        throw new NullFormFieldValueException(formField);
                    if (formField.getDefaultValue() != null)
                        submittedValues.put(formField.getName(), formField.getDefaultValue());
                }
                else submittedValues.put(formField.getName(), values.get(formField.getName()));
            }
        );
        return submittedValues;
    }
}
