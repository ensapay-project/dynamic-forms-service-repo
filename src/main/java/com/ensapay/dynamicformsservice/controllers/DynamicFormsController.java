package com.ensapay.dynamicformsservice.controllers;

import com.ensapay.dynamicformsservice.entities.Creance;
import com.ensapay.dynamicformsservice.entities.FormField;
import com.ensapay.dynamicformsservice.entities.FormSubmission;
import com.ensapay.dynamicformsservice.respostiories.CreanceRepository;
import com.ensapay.dynamicformsservice.respostiories.FormSubmissionRepository;
import com.ensapay.dynamicformsservice.services.FormValuesMapper;
import lombok.AllArgsConstructor;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/forms")
@AllArgsConstructor
public class DynamicFormsController {

    private CreanceRepository creanceRepository;
    private FormSubmissionRepository formSubmissionRepository;

    private FormValuesMapper formValuesMapper;

    @GetMapping("{id}")
    public Creance getFormFields(@PathVariable int id) {
        return creanceRepository.findById(id)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Formulaire introuvable."));
    }

    @PostMapping("{id}")
    public ResponseEntity<Creance> createForm(@PathVariable int id, @RequestBody List<FormField> fields) {
        Creance creance = creanceRepository.findById(id).orElse(new Creance(id, null));
        creance.setFields(fields);
        return ResponseEntity.ok(creanceRepository.save(creance));
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteForm(@PathVariable int id) {
        creanceRepository.delete(getFormFields(id));
    }

    @PostMapping("{id}/submit")
    public ResponseEntity<FormSubmission> submitFormValues(@PathVariable int id, @RequestBody HashMap<String, Object> values, HttpServletRequest request) throws Exception {

        KeycloakAuthenticationToken principal = (KeycloakAuthenticationToken) request.getUserPrincipal();

        FormSubmission submission = formSubmissionRepository
            .findById(new FormSubmission.FormSubmissionKey(id, principal.getName()))
            .orElse(new FormSubmission(new FormSubmission.FormSubmissionKey(id, principal.getName())));

        submission.setSubmittedValues(formValuesMapper.mapFormFieldsToValues(id, values));
        submission.setSubmissionDate(LocalDate.now());

        return ResponseEntity.ok(formSubmissionRepository.save(submission));
    }

    @GetMapping("{id}/submissions")
    public List<FormSubmission> getFormSubmissions(@PathVariable int id, HttpServletRequest request) {
        KeycloakAuthenticationToken principal = (KeycloakAuthenticationToken) request.getUserPrincipal();

        if (principal.getAccount().getRoles().contains("ROLE_agent"))
            return formSubmissionRepository.findAllByKey_CreanceId(id);
        else return List.of(
            formSubmissionRepository.findById(new FormSubmission.FormSubmissionKey(id, principal.getName()))
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND))
        );
    }

    @DeleteMapping("{id}/submissions")
    @ResponseStatus(HttpStatus.OK)
    public void deleteFormSubmissions(@PathVariable int id, HttpServletRequest request) {
        formSubmissionRepository.deleteAll(getFormSubmissions(id, request));
    }
}
