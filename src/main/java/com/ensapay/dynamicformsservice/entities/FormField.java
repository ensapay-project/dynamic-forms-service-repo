package com.ensapay.dynamicformsservice.entities;

import com.ensapay.dynamicformsservice.constants.FormFieldTypes;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Data
@Builder
public class FormField {

    private String name;
    private FormFieldTypes type = FormFieldTypes.TEXT;
    private String placeholder;
    private Object defaultValue;
    private int min, max;

    private boolean isRequired;

}
