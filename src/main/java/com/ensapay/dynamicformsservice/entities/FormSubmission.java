package com.ensapay.dynamicformsservice.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class FormSubmission {
    @Id
    private FormSubmissionKey key;
    private Map<String, Object> submittedValues;

    private LocalDate submissionDate;

    public FormSubmission(FormSubmissionKey formSubmissionKey) {
        this.key = formSubmissionKey;
    }

    @Data
    @AllArgsConstructor
    public static class FormSubmissionKey implements Serializable {
        private int creanceId;
        private String userId;
    }
}
