package com.ensapay.dynamicformsservice.entities;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@Data
@Builder
public class Creance {

    private int id;

    private List<FormField> fields;

}
