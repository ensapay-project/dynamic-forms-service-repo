package com.ensapay.dynamicformsservice.exceptions;

import com.ensapay.dynamicformsservice.entities.FormField;

public class NullFormFieldValueException extends RuntimeException {
    public NullFormFieldValueException(FormField field) {
        super(field.getName() + " is required.");
    }
}
