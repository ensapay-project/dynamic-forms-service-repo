package com.ensapay.dynamicformsservice.respostiories;

import com.ensapay.dynamicformsservice.entities.Creance;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CreanceRepository extends MongoRepository<Creance, Integer> {
}
