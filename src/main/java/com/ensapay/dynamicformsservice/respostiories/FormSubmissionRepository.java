package com.ensapay.dynamicformsservice.respostiories;

import com.ensapay.dynamicformsservice.entities.FormSubmission;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface FormSubmissionRepository extends MongoRepository<FormSubmission, FormSubmission.FormSubmissionKey> {
    List<FormSubmission> findAllByKey_CreanceId(int factureId);
}
