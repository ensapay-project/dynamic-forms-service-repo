package com.ensapay.dynamicformsservice.constants;

public enum FormFieldTypes {
    TEXT,
    NUMBER
}
