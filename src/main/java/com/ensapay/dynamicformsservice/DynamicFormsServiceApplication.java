package com.ensapay.dynamicformsservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DynamicFormsServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(DynamicFormsServiceApplication.class, args);
	}
}
