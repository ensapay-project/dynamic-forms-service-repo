# ENSAPAY Dynamic Forms Service

Dynamic Forms Service is a service for creating dynamic forms for different payments procedures. 

## Requirments
Please install these requirements before setting up the porject on your local machine :

| Requirement | DESCRIPTION                                                                         |
| ----------- | ----------------------------------------------------------------------------------- |
| JDK 8       | At least JDK 8 is required to compile the services                                  |
| Maven       | Java application packaging system   |
| MongoDB  | NoSQL Database                           |
| Consul    | Discovery and config server for services |
| Docker    | For deploying in Docker |

## Documentation

All APIs are documented with Swagger, just navigate to:
```sh
http://localhost:8085/swagger-ui/
```
## Deloyment
To deploy in Docker, just run `deploy.sh` script.

## Author

LAHMIDI Oussama

