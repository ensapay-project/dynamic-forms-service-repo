cd "${0%/*}"
echo "Building dynamic-forms-service service"
# -t for enabling tests.
if [[ "$1" == -t || $2 == -t ]]
then
mvn install package
else
mvn install package -DskipTests
fi
mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)
docker build -t dynamic-forms-service:latest .
if [[ "$1" == -d || "$2" == -d ]]
then
echo "Deploying dynamic-forms-service service"
docker-compose up
fi
